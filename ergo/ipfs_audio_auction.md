# Audio NFTs on Ergo

- Quick start
- What tools?
- Why IPFS
- Integrating with Cardano?
- Open questions

## Run an Ergo Node


## Upload to IPFS
Step 1: Connect
https://docs.ipfs.io/
https://docs.ipfs.io/how-to/command-line-quick-start/

Step 2: Open localhost:5001 and import a file

Step 3: Check that this link works (don't shut down too soon...)


## Create NFT
Step 4: If needed, let's fire up Ergo...
Start here: https://ergoplatform.org/en/blog/2019_12_02_how_to_setup/

run `java -jar ergo-<release>.jar --mainnet -c ergo.conf`
`java -jar ergo-4.0.8.jar --mainnet -c ergo.conf`

Step 5: Set up wallet on local Ergo Node Interface

Step 6: Head over to [Ergo Utils](https://ergoutils.org/#/token)
- Create NFT
- After a few minutes you'll have an NFT ID and an Artwork Checksum for your audio/

Step 7: Create auction with NFT ID
- If everything is working, you'll see a list of NFT IDs associated with your wallet.
- After you list, this NFT is no longer "yours" - see your wallet?