# This script creates, signs, and submits a transaction that sends some native assets.
# It uses the output of the transaction from update-4.sh.

SOURCE_ADDRESS=addr1q93uxc560r2jpcdqlglwqpymf5x8segrgnq020vnm6npv2622zwa23k9s7hldxl05s0ujgf0wuayf7cqfyqg4y8yth2q00l33g
DESTINATION_ADDRESS=addr1q94fk77q9lev4qw3kr3rzng23lrp2y5qxqecfnmqphmllgjch9ejpq86gmx8mdq4eup3cn7jcyvzhfke7vggn2gdjlcq776yfv
PAYMENT_SKEY="/opt/cardano/cnode/priv/wallet/wallet001/payment.skey"
TOKEN_POLICY=ddc6c804c42c61176e395b466cc57d80ce7a42d4acc743806821dc49.daceyfields
SEND_AMOUNT=1000


cardano-cli query protocol-parameters --mainnet --mary-era --out-file protocol.json

IN_UTXO=6b5bde86ca0de4d12b150c035f64989a274be54ad4a85bd4fe2228bc387f46d9#0
LOVELACE=9818747

cardano-cli transaction build-raw --mary-era --fee 0 --tx-in $IN_UTXO --tx-out "$DESTINATION_ADDRESS+$LOVELACE+$SEND_AMOUNT $TOKEN_POLICY" --out-file tx.draft

cardano-cli transaction calculate-min-fee --tx-body-file tx.draft --tx-in-count 1 --tx-out-count 1 --witness-count 1 --byron-witness-count 0 --protocol-params-file protocol.json

TX_FEE=173069
let OUTPUT=$LOVELACE-$TX_FEE

cardano-cli transaction build-raw --mary-era --fee $TX_FEE --tx-in $IN_UTXO --tx-out "$DESTINATION_ADDRESS+$OUTPUT+$SEND_AMOUNT $TOKEN_POLICY" --out-file tx.raw

cardano-cli transaction sign --tx-body-file tx.raw --signing-key-file $PAYMENT_SKEY --mainnet --out-file tx.signed

cardano-cli transaction submit --tx-file tx.signed --mainnet