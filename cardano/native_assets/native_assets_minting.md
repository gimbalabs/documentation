# Transaction Documentation
Original: https://docs.cardano.org/projects/cardano-node/en/latest/stake-pool-operations/simple_transaction.html
Faulty Native Assets: https://developers.cardano.org/en/development-environments/native-tokens/working-with-multi-asset-tokens/
CSK 005 - RCM's minter script: https://github.com/GimbaLabs/csk-005/blob/main/csk-minter.sh


## This is an example for minting native assets

## Step 1: Set up a Policy
```
mkdir token
cd token
touch dacey_field_policy.vkey
touch dacey_field_policy.skey
cd ..
cardano-cli address key-gen --verification-key-file dacey_field_policy.vkey --signing-key-file dacey_field_policy.skey
```

## Step 2: Generate Policy key-hash
```
POLICY_SKEY="dacey_field_policy.skey"
POLICY_VKEY="dacey_field_policy.vkey"
KEYHASH=$(cardano-cli address key-hash --payment-verification-key-file ${POLICY_VKEY})
```

## Step 3: Write Policy Script
In `dacey_field_policy.script`:
```
{
  "keyHash": "${KEYHASH}",
  "type": "sig"
}
```

## Step 4: Generate policyid
```
POLICY_SCRIPT="dacey_field_policy.script"
POLICY_ID=$(cardano-cli transaction policyid --script-file ${POLICY_SCRIPT})

```

## Step 5: Set some variables from funding wallet
Get each of the following from the active wallet
SENDER=$(cat "/opt/cardano/cnode/priv/wallet/wallet001/base.addr")
SENDER_SKEY="/opt/cardano/cnode/priv/wallet/wallet001/payment.skey"

## Step 6: Get protocol parameters

cardano-cli query protocol-parameters --mainnet --mary-era --out-file protocol.json

Check out that file!

## Step 7: Get info about the UTXO
Run:
```
cardano-cli query utxo --address $SENDER --mainnet --mary-era
```

Then set these values accordingly:
```
TX_IN="TxHash#TxIx"
LOVELACE=Amount (of lovelace)
```

Next step: revisit https://github.com/GimbaLabs/csk-005/blob/main/csk-minter.sh and automate this step

## Step 8: Draft the transaction

```
cardano-cli transaction build-raw --mary-era \
--fee 0 \
--tx-in $TX_IN \
--tx-out "$SENDER+$LOVELACE+1000 $POLICY_ID.daceyfields" \
--mint "1000 $POLICY_ID.daceyfields" \
--out-file tx.draft
```
               
## Step 9: Calculate Fees
cardano-cli transaction calculate-min-fee --tx-body-file tx.draft --tx-in-count 1 --tx-out-count 1 --witness-count 2 --byron-witness-count 0 --protocol-params-file protocol.json

## Step 10: Do the math
Set the transaction cost from Step 9 as TX_FEE.
$LOVELACE is initial balance:

```
let TX_OUTPUT="$LOVELACE - $TX_FEE"
echo $TX_OUTPUT
```

## Step 11: Build Actual Tx
```
cardano-cli transaction build-raw --mary-era \
--fee $TX_FEE \
--tx-in $TX_IN \
--tx-out="$SENDER+$TX_OUTPUT+1000 $POLICY_ID.daceyfields" \
--mint="1000 $POLICY_ID.daceyfields" \
--out-file finaltx.raw
```

## Step 11: Sign it
```
cardano-cli transaction sign \
--tx-body-file finaltx.raw \
--signing-key-file $SENDER_SKEY \
--signing-key-file $POLICY_SKEY \
--script-file $POLICY_SCRIPT \
--mainnet \
--out-file tx.signed
```

## Step 12: Submit it!
```
cardano-cli transaction submit \
--tx-file tx.signed \
--mainnet
```
