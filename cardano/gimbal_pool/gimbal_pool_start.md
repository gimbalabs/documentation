# GMBL Pool Documentation
## How we used Kubernetes to set up GimbalPool

## Blog outline
- Reasoning for containers
- Location
    - looked at [this map](./pool-location.png) and noticed that there are not yet any stake pools running in Scaleway Poland. So we chose that!
- why this approach?
- don't trust, verify

## Prerequisites:
1. Local Device: cardano-node, cardano-cli, cntools
    - Get familiar with cardano-node and cardano-cli: https://docs.cardano.org/projects/cardano-node/en/latest/getting-started/install.html
    - You can also use the Docker image - a container. We believe in knowing a big about what you're abstracting
    - 
2. Local Device: kubernetes
    - install kubernetes
    - install kustomize
    - install kapp
3. Cloud hosting account with Kubernetes. We chose Scaleway, mostly for the geography, but also because they're a thoughtful small cloud host their vision aligns with ours.

## With Prerequisites in place, here is the workflow:
### Create wallet and pool in CNtools or via cardano-cli
4. Use cntools and cardano-cli to generate cold keys, etc. Links to documentation
6. Registration, metadata, and transactions (see discord chat)
### Create Kubernetes cluster on cloud host
    - What is the minimum requirement for the block producing node? For each relay?
      - 6-8gb for mainnet relays, a bit less of RAM needed for block producer since they don't handle peers connections
    - Minimum vs what you can change later
### Set KUBECONFIG locally
3. Interact with your k8s cluster remotely
### Use kustomize-cardano-node
5. Use [kustomize-cardano-node](https://github.com/repsistance/kustomize-cardano-node) to start your nodes
    - additional docs here, or via github?
    - should we host on gitlab?
    - special links from gimbalabs.com?
    - docs for kustomize-cardano-node
### Check your node
7. How to interact, make sure it's all working



- What about running a relay node locally? No harm right, I can try to keep it running, and if it fails, it's replaced?
- When does the database sync and where does it go? What about upon restart?
- Show what's really happening when you register your delegation
https://docs.cardano.org/projects/cardano-node/en/latest/stake-pool-operations/register_key.html

## Additional Links:
- pool metadata: 
